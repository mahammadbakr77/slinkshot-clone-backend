const router = require('express').Router()
const authMiddleware = require('../Middlewares/Auth')
const auth = require('./auth')
const user = require('./user')


const {
    getAllSkins,getAllSlinkShots,
    newSkin,newSlinkShot,newUserDetails,newFollowerForUserDetails,newSkinForUserDetails,newSlinkShotForUserDetails,
    getUserDetailsById,updateUserDetailsById,updateSlinkShotById,addViewForSlinkShotById,addLikeForSlinkShotById
} = require('../Controller/Controller')

//Auth
router.use('/auth', auth)
router.use('/user', authMiddleware)
router.use('/user', user)

//Other
router.use('/getAllSkins', getAllSkins)
router.use('/getAllSlinkShots', getAllSlinkShots)
router.use('/newSkin', newSkin)
router.use('/newSlinkShot', newSlinkShot)
router.use('/newUserDetails', newUserDetails)
router.use('/newFollowerForUserDetails', newFollowerForUserDetails)
router.use('/newSkinForUserDetails', newSkinForUserDetails)
router.use('/newSlinkShotForUserDetails', newSlinkShotForUserDetails)
router.use('/getUserDetailsById', getUserDetailsById)
router.use('/updateUserDetailsById', updateUserDetailsById)
router.use('/updateSlinkShotById', updateSlinkShotById)
router.use('/addViewForSlinkShotById', addViewForSlinkShotById)
router.use('/addLikeForSlinkShotById', addLikeForSlinkShotById)



module.exports = router